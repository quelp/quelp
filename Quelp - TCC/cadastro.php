<?php
    require_once 'classes/usuarios.php';
    $u = new Usuario;
?>

<html lang="pt-br">
    <head>
        <meta charset="utf-8"/>
        <title>Quelp-TCC</title>
        <link rel="stylesheet" href="css/estilo.css">
    </head>
    <body>
    <div id="corpo-form-cad">
        <h1>Cadastrar</h1>
        <form method="post">
            <input type="text" name="nome" placeholder="Nome completo" maxlength="30">
            <input type="email" name="email" placeholder="E-mail" maxlength="40">
            <input type="password" name="senha" placeholder="Senha" maxlength="15">
            <input type="password" name="confsenha" placeholder="Confirmar senha" maxlength="15">
            <input type="submit" value="Cadastrar">
            <a href="login.php">Já é cadastrado?<strong> Faça login</strong></a>
        </form>
    </div>

<?php
//verificar se clicou no botao
if(isset($_POST['nome']))
{
	$nome = addslashes($_POST['nome']); //addslashes evita codigos maliciosos.
	$email = addslashes($_POST['email']);
	$senha = addslashes($_POST['senha']);
	$confirmarSenha = addslashes( $_POST['confsenha']);
	//verificando se todos os campos nao estao vazios
	if(!empty($nome) && !empty($email) && !empty($senha) && !empty($confirmarSenha))
	{
		$u->conectar("banco_tcc","localhost","root","");  //Config  dbname pass local
		if ($u->msgErro == "") //conectado normalmente;
		{
			if ($senha == $confirmarSenha)
			{
				if ($u->cadastrar($nome, $email, $senha))
				{
					?>
                    <div id="msg-sucesso">
                    Cadastro realizado com sucesso!
                    </div>
                    <?php
				}
				else
			 	{
			 		?>
                    <div class="msg-erro">
                        Email já cadastrado, retorne e faça login.
                    </div>
                    <?php
			 	}
			}
			else
			{
                ?>
                <div class="msg-erro">
                    Senhas não conferem!
                </div>
                <?php
			}
		}
		else
		{
           ?>
           <div class="msg-erro">
              <?php echo "Erro: ".$u->msgErro; ?>
           </div>
            <?php
		}
	}
	else
		{
            ?>
            <div class="msg-erro">
                Preencha todos os campos!
            </div>
                <?php
		}
}
?>


    </body>
</html>
