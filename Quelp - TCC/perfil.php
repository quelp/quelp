<?php
require_once("navbar.php");
include_once("conexao.php");
$id = $_SESSION['id_usuario'];
$result_usuario = "SELECT * FROM usuarios WHERE id_usuario = '$id'";
$resultado_usuario = mysqli_query($conexao, $result_usuario);
$row_usuario = mysqli_fetch_array($resultado_usuario);
?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Perfil</title>		
	</head>
	<body>
		<div class="container h-100">
  		<div class="row h-100 justify-content-center align-items-center">
			<form method="post" action="editperfil.php">
				<input type="hidden" name="id_usuario" value="<?php echo $row_usuario['id_usuario']; ?>">
				<label>Nome: </label>
				<input type="text" name="nome" placeholder="Digite o nome completo" value="<?php echo $row_usuario['nome']; ?>"><br>
				<label>E-mail: </label>
				<input type="email" name="email" placeholder="Digite o seu e-mail" value="<?php echo $row_usuario['email']; ?>"><br>
				<label>Senha: </label>
				<input type="password" name="senha" placeholder="Digite sua senha antiga"><br>
				<label>Nova senha: </label>
				<input type="password" name="novasenha" placeholder="Digite sua nova senha"><br>
				<input type="submit" value="Editar">
			</form>
  		</div>
		</div>
	</body>
</html>