<?php

class Usuario
{
    private $pdo;
    public $msgErro = "";
        
    public function conectar($nome, $host, $usuario, $senha)
    {
        global $pdo;
        global $msgErro;
        try 
        {
            $pdo = new PDO("mysql:dbname=".$nome.";host=".$host,$usuario,$senha);
        } catch (PDOException $e){
            $msgErro = $e->getMessage (); 
        }
        
    }
    
    public function cadastrar($nome,$email,$senha)
    {
        global $pdo;
        global $msgErro;
        global $email;
        global $nome;
        global $senha;
        //verificar se o email ja esta cadastrado
        $sql = $pdo->prepare("SELECT id_usuario FROM usuarios WHERE email = :e");
        $sql->bindValue(":e",$email);
        $sql->execute();
        if($sql->rowCount() > 0)
        {
            return false; //ja esta cadastrado
        }
        else
        {
        //se não estiver,cadastrar
        $sql = $pdo->prepare("INSERT INTO usuarios (nome, email, senha) VALUES (:n, :e, :s)");
        $sql->bindValue(":n",$nome);
        $sql->bindValue(":e",$email);
        $sql->bindValue(":s",md5($senha));
        $sql->execute();
        return true; //tudo ok
        }
    }
    
    public function logar()
    {
        global $pdo;
        global $msgErro;
        global $email;
        global $nome;
        global $senha;
        //verficiar se email e senha estao cadastrados, se sim
        $sql = $pdo->prepare("SELECT id_usuario FROM usuarios WHERE email = :e AND senha = :s");
        $sql->bindValue(":e",$email);
        $sql->bindValue(":s",md5($senha));
        $sql->execute();
        if($sql->rowCount() > 0 )
        {
            //entrar no sistema(sessao)
            $dado = $sql->fetch();
            session_start();
            $_SESSION['id_usuario'] = $dado['id_usuario'];
            return true; //login feito com sucesso 
        }
        else
        {
            return false; //nao foi possivel fazer login 
        }
        
    }
}

?>