<?php
    session_start ();
    require_once 'classes/usuarios.php';
    $u = new Usuario;
?>
<html lang="pt-br">
    <head>
        <meta charset="utf-8"/>
        <title>Login</title>
        <link rel="stylesheet" href="css/estilo.css">
    </head>
    <body>
    <div id="corpo-form">
        <h1>Login</h1>
        <form method="post">
            <input type="email" name="email" placeholder="E-mail">
            <input type="password" name="senha" placeholder="Senha">
            <input type="submit" value="Logar">
            <a href="cadastro.php">Ainda não é cadastrado?<strong> Cadastre-se!</strong></a>
        </form>
    </div>      
<?php
	if(isset($_POST['email']))
	{
		$email = addslashes($_POST['email']);
		$senha = addslashes($_POST['senha']);
		//verificando se todos os campos nao estao vazios
		if(!empty($email) && !empty($senha))
		{
			$u->conectar("banco_tcc","localhost","root",""); //conectando ao banco 
			if($u->msgErro=="") // caso a mensagem esteja vazia, login ok
			{
				if ($u->logar($email,$senha))
				{
                    $_SESSION['logar'] = 1;
					header("location:index.php"); //encaminhado para proxima area apos verificar usuario e senha
				}
				else
				{
					?>
					<div class="msg-erro">
						Email e/ou senha estão incorretos!
					</div>
					<?php
				}
			}
			else
			{
				?>
				<div class="msg-erro">
					<?php echo "Erro: ".$u->msgErro; ?>
				</div>
				<?php
			}
		}
		else
		{
      			?>
			<div class="msg-erro">
				Preencha todos os campos!
			</div>
			<?php
		}
	}
	?>
    </body>
</html>