<!DOCTYPE html>
<?php
    session_start();
    require_once 'classes/usuarios.php';
?>
<?php
include("conexao.php");
?>


<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/estilo.css">
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <meta charset="utf-8"/>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</head>

<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#">Quelp</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item">
          <a class="nav-link" href="index.php">Home</a>
        </li>
        <?php if(isset ($_SESSION['logar'])){ ?>
        <li class="nav-item">
          <a class="nav-link" href="paginadousuario.php">Perfil</a>
        </li>
        <?php }else{} ?>
        <?php if(isset ($_SESSION['logar'])){ ?>
          <li class="nav-item">
          <a class="nav-link" href="perguntas.php">Faça sua pergunta</a>
        </li>
          <?php }else{} ?>
      </ul>
      <form class="form-inline my-2 my-lg-0">
          <?php
          if (!isset ($_SESSION['logar'])){?>
              <a href="login.php" class="btn btn-outline-success">Login</a>
          <?php
          } if(isset ($_SESSION['logar'])){ 
               $usuario = $_SESSION['id_usuario'];
$result_usu = "SELECT * FROM usuarios WHERE id_usuario = $usuario";
$resultado_usu = mysqli_query($conexao, $result_usu);
$row_usuario = mysqli_fetch_array($resultado_usu);
          ?>
              <div class="dropdown">
              <button class="btn btn-outline-success dropdown-toggle btPerfil" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo $row_usuario['nome']; ?></button>
              <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
              <a class="dropdown-item" href="perfil.php">Editar Perfil</a>
              <a class="dropdown-item" href="sair.php">Sair</a>
              </div>
              </div>
          <?php } ?>
      </form>
    </div>
  </nav>
</html>