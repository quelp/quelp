<!DOCTYPE html>
<?php
    session_start();
    require_once 'classes/usuarios.php';
?>

<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <meta charset="utf-8"/>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </head>
<body>
  <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="index.php">Quelp</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav mr-auto">
      </ul>
      <a href="index.php" class="btn btn-outline-success">Voltar</a>
      <form class="form-inline my-2 my-lg-0">
          <?php
		  if(!empty($_SESSION['email']) && !empty($_SESSION['senha'])){ ?>
                <a href="sair.php" class="btn btn-outline-success">Sair</a>
          <?php
          } else{
          if (!isset ($_SESSION['logar'])){?>
              <a href="login.php" class="btn btn-outline-success">Login</a>
          <?php
          } if(isset ($_SESSION['logar'])){ ?>
              <div class="dropdown">
              <button class="btn btn-outline-success dropdown-toggle btPerfil" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Perfil</button>
              <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
              <a class="dropdown-item" href="perfil.php">Editar Perfil</a>
              <a class="dropdown-item" href="sair.php">Sair</a>
              </div>
              </div>
          <?php } }?>
      </form>
    </div>
  </nav>
  <div class="container h-100">
    <div class="row h-100 justify-content-center align-items-center">
      <form method="post" action="mandaperguntas.php">
        <h2>Tira todas as suas dúvidas aqui</h2>
        <p>Titulo da pergunta: <input type="text" size="49" maxlength="300" name="titulopgt"></p>
        <p>Digite sua dúvida:
        <p><textarea name="pergunta" cols="70" rows="10" placeholder="Digite aqui sua dúvida.(Para obter uma melhor resposta descreva sua dúvida de forma clara, simples e objetiva)." maxlength="40000"></textarea>
        <p>Escolha a categoria:
        <select name="categoria">
        <option></option>
        <option value="conhecimentosgerais">Conhecimentos gerais</option>
        <option value="portugues">Português</option>
        <option value="matematica">Matemática</option>
        <option value="historia">História</option>
        <option value="geografia">Geografia</option>
        <option value="sociologia">Sociologia</option>
        <option value="tecnologia">Tecnologia</option>
        <option value="biologia">Biologia</option>
        <option value="progamacao">Programação</option>
        <option value="bancodedados">Banco de dados</option>
        </select>
        </p>
        <p><input type="submit" value="ENVIAR PERGUNTA">
      </form>
    </div>
  </div>
</body>
</html>
