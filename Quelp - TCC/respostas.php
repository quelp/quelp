<?php
require_once("navbar.php");
?>

<html>
<head>
<meta charset=utf-8>
</head>
<body>
<div class="container h-100">
    <div class="row h-100 justify-content-center align-items-center">
      <form method="post" action="mandarespostas.php">
        <h2>Envie uma resposta para a pergunta aqui</h2>
        <p>Digite sua resposta:
        <p><textarea name="resposta" cols="70" rows="10" placeholder="Digite aqui sua resposta para a pergunta.(Para um entendimento geral de sua respota descreva ela de forma clara, simples e objetiva)." maxlength="40000"></textarea>
        <p><input type="submit" value="ENVIAR RESPOSTA">
      </form>
    </div>
  </div>
</body>
</html>