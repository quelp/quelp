---
marp: true
theme: cedup
style: |
    header, footer {
    }
    section.lead {
        background: linear-gradient(#222, #292929);
    }
    section{
        background-image: url("slidebackground.png");
    }
    h6 {
    font-size: 0.7em;
    }
    blockquote {
        font-size: 0.7em;
    }
    th {
        background-color: #333;
        color: #FFF;
    }
class:
  - lead
  - invert
---

<!-- 
header: Centro de Educação Profissional Abílio Paulo
footer: Projeto Integrado de Desenvolvimento do Curso de Ensino Médio Integrado em Informática 
-->

![bg right:35% sepia](img/background.jpg)

# Quelp
## Protótipo de website com o objetivo de ajudar na solução de dúvidas

Guilherme Scarpatto da Silva
Daniel Bonfante Ito Marim

###### *Criciúma, Novembro de 2019*

---

<!--
paginate: true
class:
-->

## Agenda

1. Introdução e Justificativa
1. Projeto de Software
    1. Análise de Requisitos
    1. Modelagem de Dados
1. Software Desenvolvido
    1. Linguagens e Persistência de Dados
    1. Frameworks, Ferramentas e Recursos
1. Demonstração
1. Conclusão e Agradecimentos

---

## Introdução

![bg right:30%](img/background.jpg)

* Solução de dúvidas;
* Aumento de conhecimento;
* Fóruns;

---

## Justificativa


![bg right:30%](img/background.jpg)

* Mercado amplo;
* Aumento uso da internet;
* Solução de dúvidas;
* Busca por conhecimento;

---

## Projeto de Software

![bg right:50%](img/background.jpg)

---

### Análise de Requisitos

Requisitos Funcionais

| Requisito | Descrição |
| -- | -- |
| Listar perguntas | Mostra as perguntas feitas pelos usuarios, juntamente a seus nomes de forma listada.|
| Listar respostas | Mostra as respostas em baixo de sua respectiva pergunta juntamente ao nome do autor da resposta.|
| Cadastro | O usuário faz seu cadastro para que possa usar mais funções no site.|
| Perfil | Tela que mostra as perguntas feita pelo usuario.|
| Edição de perfil | Tela onde o usuario pode editar seus dados de cadastro.|

---

### Análise de Requisitos

Requisitos Não-Funcionais

| Requisito | Descrição |
| -- | -- |
| Banco de Dados | O protótipo terá comunicação com o banco de dados.|
| Cadastro Obrigatório | Apenas os usuários cadastrados poderão realizar perguntas, respostas.|
| Visualização de perguntas | As perguntas e respostas poderão ser visualizadas mesmo que o usuario não seja cadastrado.|
| Linguagens | O website foi desenvolvido em php, css, html e bootstrap.|
---

### Modelagem de Dados

![bg right:50%](img/background.jpg)

---

#### Diagrama de Caso de Uso

![bg left:70% fit](img/casouso.png)

---

#### Diagrama de Classe

![bg right:70% fit](img/classe.png)

---

## Software Desenvolvido

![bg right:60%](img/soft_desenvolvido.png)

---

### Linguagens Usadas

![bg left:30% fit](img/linguagem02.png)
![bg left:30% fit](img/linguagem01.png)
![bg left:30% fit](img/linguagem04.png)
![bg left:30% fit](img/linguagem03.png)

* HTML
* CSS
* PHP
* Javascript

---

### Persistência de Dados

![bg left:30% fit](img/banco.png)

* MySQL

---

### Frameworks e Ferramentas

![bg left:30% fit](img/ferramenta.png)
![bg left:30% fit](img/ferramenta2.png)
![bg left:30% fit](img/framework.jpg)

* VSCode
* Bootstrap
* Brackets

---

### Recursos Utilizados

![bg right:30%](img/background.jpg)

* Notebook I5 5º geração, 6GB ram, 1TB armazenamento
* Fóruns
* Tempo

---

## Testes

* Foram realizados alguns testes com diferentes pessoas, colegas de turma e parentes de casa,  eles avaliaram o projeto positivamente e apontaram alguns erros e possiveis atualizações.

![bg right:30%](img/background.jpg)

---

## Demonstração

---

## Conclusões

![bg right:30%](img/background.jpg)


* Mais conhecimento das linguagens usadas;
* Objetivo principal atingido;
* Administração do tempo;

---

## Projetos Futuros

![bg right:30%](img/background.jpg)


* Adicionar novas funcões;
* Modificar visual deixando mais interativo;
* Criação de logo;
* Hospedagem;

---

## Agradecimentos

![bg right:40% fit](img/aluno01.jpg)
![bg vertical fit](img/aluno02.jpg)

* Agradecer principalmente aos colegas e familiares que colaboraram com conselhos, dicas, motivações.

---

![bg left:30% fit](img/cedup.png)

# Obrigado pela atenção!
## Dúvidas?

Guilherme Scarpatto da Silva
Daniel Bonfante Ito Marim

> *Todas as imagens usadas nesta apresentação são de domínio público, dos autores do trabalho ou propriedade de suas respectivas marcas*