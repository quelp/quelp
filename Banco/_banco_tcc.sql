-- phpMyAdmin SQL Dump
-- version 4.1.4
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 28-Nov-2019 às 20:46
-- Versão do servidor: 5.6.15-log
-- PHP Version: 5.4.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `banco_tcc`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `perguntas`
--

CREATE TABLE IF NOT EXISTS `perguntas` (
  `id_pergunta` int(11) NOT NULL AUTO_INCREMENT,
  `pergunta` longtext NOT NULL,
  `fk_usuario` int(11) NOT NULL,
  `categoria` int(11) NOT NULL,
  `titulo` text NOT NULL,
  PRIMARY KEY (`id_pergunta`),
  KEY `fk_usuario` (`fk_usuario`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=27 ;

--
-- Extraindo dados da tabela `perguntas`
--

INSERT INTO `perguntas` (`id_pergunta`, `pergunta`, `fk_usuario`, `categoria`, `titulo`) VALUES
(23, 'queria resolver uma conta que tenho duvida, segue abaixo o problema:\r\n1+2x3+2\r\n', 8, 0, 'Preciso de ajuda numa conta matematica'),
(25, 'estou fazendo um trabalho sobre o cantor e preciso saber a data de nascimento e falecimento do artista', 8, 0, 'Com quantos anos John Lennon morreu?'),
(26, 'Preciso saber quais sÃ£o as estruturas de repetiÃ§Ã£o mais recomendadas, preciso para meu novo projeto do tcc, agradeÃ§o desde jÃ¡ jovens;\r\n\r\nzininho', 9, 0, 'Ajuda, urgente!!');

-- --------------------------------------------------------

--
-- Estrutura da tabela `respostas`
--

CREATE TABLE IF NOT EXISTS `respostas` (
  `id_resposta` int(11) NOT NULL AUTO_INCREMENT,
  `resposta` longtext NOT NULL,
  `fk_pergunta` int(11) NOT NULL,
  `fk_usuario` int(11) NOT NULL,
  PRIMARY KEY (`id_resposta`),
  KEY `fk_pergunta` (`fk_pergunta`),
  KEY `fk_usuario` (`fk_usuario`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=43 ;

--
-- Extraindo dados da tabela `respostas`
--

INSERT INTO `respostas` (`id_resposta`, `resposta`, `fk_pergunta`, `fk_usuario`) VALUES
(39, 'Bom dia professor, normalmente eu uso o IF ou o WHILE, acho que sÃ£o as estruturas mais faceis de usar, espero ter ajudado.', 26, 8),
(38, '2x3=6; 6+2=8; 8+1=9.', 23, 9),
(37, 'ele nasceu em 1940 e faleceu em 1980, com 40 anos', 25, 8),
(40, 'Resolva primeiro a multiplicaÃ§Ã£o pra depois a soma, ficando assim entÃ£o 1+6+2=7+2=9.', 23, 10),
(41, '40 anos, nasceu no ano de 1940 e morreu no ano de 1980', 25, 10),
(42, 'Geralmente sÃ£o usadas as estruturas IF ou WHILE', 26, 10);

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios` (
  `id_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(30) DEFAULT NULL,
  `email` varchar(40) DEFAULT NULL,
  `senha` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id_usuario`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Extraindo dados da tabela `usuarios`
--

INSERT INTO `usuarios` (`id_usuario`, `nome`, `email`, `senha`) VALUES
(7, 'gui', 'gui@gmail.com', 'd41d8cd98f00b204e9800998ecf8427e'),
(8, 'Daniel Ito', 'a@gmail.com', '202cb962ac59075b964b07152d234b70'),
(9, 'rVenson', 'r@gmail.com', '202cb962ac59075b964b07152d234b70'),
(10, 'Guilherme Scarpatto', 'gui2@gmail.com', '202cb962ac59075b964b07152d234b70');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
